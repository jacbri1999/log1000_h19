#include "mecanicienne.h"

Mecanicienne::Mecanicienne(int matricule){
    this->matricule = matricule;
}

void Mecanicienne::assigneeSur(Voiture* une_voiture){
    voitures_assignees.push_back(une_voiture);
}

void Mecanicienne::aTermineAvec(Voiture* une_voiture) {
    voitures_assignees.remove(une_voiture);
}

int Mecanicienne::getMatricule() {
    return this->matricule;
}

void Mecanicienne::afficheAssignations() {
    std::list<Voiture*>::iterator va;
    for (va = voitures_assignees.begin(); va != voitures_assignees.end(); va++) {
        std::cout << "    Voiture " << (*va)->getID() << std::endl;
    }
}
